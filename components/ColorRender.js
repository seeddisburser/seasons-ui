import { useState } from 'react'

export default function ColorRender(ra, rb, ga, gb, ba, bb, month, day) {
    // defines current date
    var date = ((month + 1) * 30) + day

    const decimalAdjust = (type, value, exp) => {
        // If the exp is undefined or zero...
        if (typeof exp === 'undefined' || +exp === 0) {
          return Math[type](value);
        }
        value = +value;
        exp = +exp;
        // If the value is not a number or the exp is not an integer...
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
          return NaN;
        }
        // Shift
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        // Shift back
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
      }

    // Defines place in gradient for R, G, and B values
    var colorR = ((date/365) * (rb-ra)) + ra
    var colorG = ((date/365) * (gb-ga)) + ga
    var colorB = ((date/365) * (bb-ba)) + ba

    // applies decimal floor and combines variable into a css digestible string
    var col = decimalAdjust('floor', colorR, -1) + ", " + decimalAdjust('floor', colorG, -1) + ", " + decimalAdjust('floor', colorB, -1) 

    return {
        col,
        month,
        day,
    }
        
    
}